from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
from dash.dependencies import Output, Input, State
from dash.exceptions import PreventUpdate
import dash_table
import plotly.express as px
import plotly.graph_objects as go
from hackathon_global import *

def get_bar_chart(feature_name, label):
    df = data_selected.copy()
    if not label:
        df = df
    else:
        df = df[df['label'] == label]

    fig = px.bar(df, y=df[feature_name].value_counts().values, x = df[feature_name].value_counts().index, template="plotly_dark")

    fig.update_layout(
                font_color = 'white',
                showlegend=False,
                xaxis_title='Bytes',
                yaxis={'title':''},
                margin=dict(
        l=0,
        r=0,
        b=0,
        t=0,
        pad=0
    ),
            paper_bgcolor='rgba(51, 51, 51, 1)',
            plot_bgcolor='rgba(51, 51, 51, 1)',
    )

    return dcc.Graph(figure = fig, className='plot-figure')

def get_pie_chart(feature_name, label):
    df = data_selected.copy()
    if not label:
        df = df
    else:
        df = df[df['label'] == label]

    fig = px.pie(df, values=df[feature_name].value_counts().values, names = df[feature_name].value_counts().index, template="plotly_dark")

    fig.update_layout(
                font_color = 'white',
                showlegend=False,
                xaxis_title='Bytes',
                yaxis={'title':''},
                margin=dict(
        l=0,
        r=0,
        b=0,
        t=0,
        pad=0
    ),
            paper_bgcolor='rgba(51, 51, 51, 1)',
            plot_bgcolor='rgba(51, 51, 51, 1)',
    )

    return dcc.Graph(figure = fig, className='plot-figure')

def get_hackathon_feature(col, label):
    df = data_selected.copy()
    if(label is not None):
        df = df[df["label"] == label]
    x = df[col].describe().to_dict()
    header = [
        html.Thead(html.Tr([html.Th("Stat"), html.Th(col)]))
    ]
    body = [html.Tbody([
        html.Tr([
            html.Td(key),
            html.Td(str(x[key]))
        ]) for key in x
    ])]
    return dbc.Col(dbc.Table(header+body, bordered=True))

def get_label(children, className=None):
    """ Get a label in the tool container

    Args:
        children (str): label text
        className (str): style className used

    Return:
        html.Label: a Dash component
    """
    return html.Label(children=children, className=className)

def get_features(label):
    return dbc.Container([
        dbc.Row([
            dbc.Col(html.Div([get_label('labels.testType', className='option-label-2'), get_pie_chart("labels.testType", label)], className = 'col-md-12 plot-board')),
            dbc.Col(html.Div([get_label("domains.num", className='option-label-2'), get_bar_chart("domains.num", label)], className = 'col-md-12 feature-board'))
        ]),
        dbc.Row([
            dbc.Col(html.Div([get_label("metrics.message", className='option-label-2'), get_pie_chart("metrics.message", label)], className = 'col-md-12 plot-board')),
            dbc.Col(html.Div([get_label("status", className='option-label-2'), get_bar_chart("status", label)], className = 'col-md-12 feature-board'))
        ])
    ])

def get_info(label):
    return dbc.Container([
        dbc.Row([
            dbc.Col(html.Div(get_hackathon_feature('metrics.message', label), className = 'col-md-12 info-board-blue')),
            dbc.Col(html.Div(get_hackathon_feature('labels.nPoint', label), className = 'col-md-12 info-board-pink'))
        ]),
        dbc.Row([
            dbc.Col(html.Div(get_hackathon_feature('status', label), className = 'col-md-12 info-board-pink')),
            dbc.Col(html.Div(get_hackathon_feature('metrics.IP', label), className = 'col-md-12 info-board-blue'))
        ])
    ])

def get_gis_map_result():
    df = data_selected.copy()
    temp = df[~df["metrics.IP"].isnull()]
    temp = temp[["metrics.IP", "latitude", "longitude"]].drop_duplicates().reset_index(drop=True)
    fig = go.Figure(go.Scattermapbox())
    for _, row in temp.iterrows():
        lo = [row['longitude']]
        lo.append(-76.3592)
        la = [row['latitude']]
        la.append(37.0835)
        fig.add_trace(go.Scattermapbox(
            mode="markers+lines",
            lon=lo,
            lat =la,
            name = row['metrics.IP'],
            marker = {'size': 10, 'opacity': 1},
            opacity=0.8
        ))

        fig.update_layout(
            margin ={'l':0,'t':0,'b':0,'r':0},
            mapbox = {
            'accesstoken': 'pk.eyJ1IjoibGluY2hlbnZhIiwiYSI6ImNrbHI0cnZzaTBha2wydm1pdHVoczFrdmYifQ.R40_D1qPC-tllkKrtACAow',
            'style': "dark",
            'center': {'lon': -76.342339, 'lat': 37.028271},
            'zoom': 4},
            showlegend = False)

    return html.Div([
    dcc.Graph(figure=fig)
    ])

def get_content_pa(label = 0):
    return html.Div(
        dbc.Container([
            dbc.Row([
            html.Div(get_info(label), className = 'col-md-6'),
            html.Div(get_gis_map_result(), className = 'col-md-6')
            ], className = 'row-content'),
            dbc.Row([
            html.Div(get_features(label), className = 'col-md-6'),
            html.Div(get_record_table(), className = 'col-md-6')
            ], className = 'row-content')
        ], fluid = True)
    )

def get_content_malicious(label = -1):
    return html.Div(
        dbc.Container([
            dbc.Row([
            html.Div(get_info(label), className = 'col-md-6'),
            html.Div(get_gis_map_result(), className = 'col-md-6')
            ], className = 'row-content'),
            dbc.Row([
            html.Div(get_features(label), className = 'col-md-6'),
            html.Div(get_record_table(), className = 'col-md-6')
            ], className = 'row-content')
        ], fluid = True)
    )

def get_content_all(label = None):
    return html.Div(
        dbc.Container([
            dbc.Row([
            html.Div(get_info(label), className = 'col-md-6'),
            html.Div(get_gis_map_result(), className = 'col-md-6')
            ], className = 'row-content'),
            dbc.Row([
            html.Div(get_features(label), className = 'col-md-6'),
            html.Div(get_record_table(), className = 'col-md-6')
            ], className = 'row-content')
        ], fluid = True)
    )

def get_record_table():
    """Get a record table

    Return:
        dash_table.DataTable, a Dash component
    """
    features = ['labels.nPoint', 'domains.num', 'domains.length',
       'labels.testType', 'metrics.message', 'metrics.DNS_time',
       'metrics.socket_establishment_time', 'metrics.IP', 'timestamp', 'label']

    return dash_table.DataTable(
        id='record-table',
        columns=[{"name": i, "id": i, 'selectable':True} for i in features],
        page_current=0,
        page_size=22,
        page_action='custom',
        style_table = {'color': 'silver'},
        style_cell = {'background-color': 'rgba(51, 51, 51, 1)', 'text-align':'center', 'overflow': 'hidden', 'textOverflow': 'ellipsis', 'maxWidth': 0},
        cell_selectable=False,
        sort_action='custom',
        sort_mode='multi',
        sort_by=[],
    )

@app.callback(
    Output('record-table', 'data'),
    Output('record-table', 'page_count'),
    Input('record-table', "page_current"),
    Input('record-table', "page_size"),
    Input('record-table', "sort_by"))
def update_ip_record_table(page_current, page_size, sort_by):
    features = ['labels.nPoint', 'domains.num', 'domains.length',
       'labels.testType', 'metrics.message', 'metrics.DNS_time',
       'metrics.socket_establishment_time', 'metrics.IP', 'timestamp', 'label']
    df = data_selected[features]

    if len(sort_by):
        dff = df.sort_values(
            [col['column_id'] for col in sort_by],
            ascending=[
                col['direction'] == 'asc'
                for col in sort_by
            ],
            inplace=False
        )
    else:
        # No sort is applied
        dff = df

    num = int(df.shape[0]/page_size)

    if df.shape[0]%page_size > 0:
        num += 1

    temp = dff.iloc[
        page_current*page_size:(page_current+ 1)*page_size
    ]

    return temp.to_dict('records'), num

def get_tabs():
    """Get tabs component

    Return:
        dbc.Tabs, a DBC component
    """
    return dbc.Tabs(
        [
            dbc.Tab(label="All", tab_id = 'tab-1',
                    label_style = {'color':'silver', 'text-align': 'center', 'font-weight':'bold'},
                    tab_style = {'width': '10%', 'background-color': '#1C2833', 'margin-left':'5px'},
                    active_label_style = {'color': 'white'}),
            dbc.Tab(label="Outlier", tab_id = 'tab-2',
                    label_style = {'color':'silver', 'text-align': 'center', 'font-weight':'bold'},
                    tab_style = {'width': '10%', 'background-color': '#1C2833', 'margin-left':'5px'},
                    active_label_style = {'color': 'white'}),
            dbc.Tab(label="Major", tab_id = 'tab-3',
                    label_style = {'color':'silver', 'text-align': 'center', 'font-weight':'bold'},
                    tab_style = {'width': '10%', 'background-color': '#1C2833', 'margin-left':'5px'},
                    active_label_style = {'color': 'white'}),
        ], id = 'tabs', active_tab = 'tab-1', className = 'row-content')

@app.callback(Output("content", "children"),
              Input("tabs", "active_tab"))
def output_text(at):
    global data_selected
    if at == 'tab-1':
        data_selected = data
        return html.Div(get_content_all())
    elif at == 'tab-2':
        data_selected = data[data['label'] == -1]
        return html.Div(get_content_malicious())
    elif at == 'tab-3':
        data_selected = data[data['label'] == 0]
        return html.Div(get_content_pa())

    return 'Unknown'
