from dash import html
from hackathon_title import *
from hackathon_tabs import *
from hackathon_help import *
from hackathon_global import *

def get_layout():
    return html.Div([
        get_title_container(),
        get_help(),
        get_tabs(),
        #html.Div(get_hackathon_features(column_list, None), style={'width': '50%'}, id="feature-data"),
        #get_graphs(),
        html.Div(id = 'content')
    ])
