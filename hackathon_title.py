from dash import html
import dash_bootstrap_components as dbc

def get_title_container():
    """ Get the title container

    Return:
        html.Div: a Dash component
    """
    return html.Div([
        dbc.Container([
            html.Div([
            html.Img(src='assets/USAF_logo.png', className='header-icon-l'),
            html.Div([
                html.H5("BRAVO ELICSAR MIXER", style={"color": "white", "margin-left": "10px", "margin-top": "10px"}), 
                html.H6("(BEM) Dashboard", style={"color": "white",  "margin-left": "10px"})], 
                style={"float": "left"}),
            ], className = 'title-container')
        ], fluid = True)
    ])
