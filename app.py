#--------------------------------------------------------------------------
# Air Force Bravo 01 Hackathon
# Copyright (c) 2022 USAF ACC/A29 Intel Data/Tech
# All Rights Reserved.
#
# Dissemination of this information and/or reproduction
# and modification are restricted to other  government
# organizations.  Commercial use is strictly forbidden
# unless prior written permission is obtained from ACC/A29
# Intel Data/Tech Futures Division.
#
#--------------------------------------------------------------------------
__author__ = "Lin, Sarah, Justin"
__version__ = "0.1"
__email__ = "lin.chen.1@us.af.mil"
__status__ = "Prototype"

import dash
from hackathon_global import *
from hackathon_layout import *

app.layout = get_layout()

if __name__ == '__main__':
    app.run_server(debug = True)
    #app.run_server(host='0.0.0.0', port=8082)
