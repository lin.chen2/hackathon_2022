import dash
import dash_bootstrap_components as dbc
import pandas as pd


column_list = ["status", "labels.testType", "metrics.DNS_time", "metrics.socket_establishment_time"]

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.CYBORG])
app.config['suppress_callback_exceptions'] = True
server = app.server

data = pd.read_csv('../ELICSAR_Traffics_Location.csv')

data_selected = None
