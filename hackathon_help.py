from dash import html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
from hackathon_global import *

def get_help():
    """Get help information for training page

    Return:
        html.Div, a Dash component
    """
    return html.Div(
    [
        dbc.Button("?", id="button-help", color="secondary", outline = True, size = 'sm', className = 'id-help'),
        dbc.Offcanvas(
            [
                html.Label('Goals', className = 'canvas-section-title'),
                html.P("1. Cluster data to identify potential patterns", className = 'canvas-paragraph'),
                html.P("2. Identify outliers", className = 'canvas-paragraph'),
                html.Label('Data', className = 'canvas-section-title'),
                html.P("ELICSAR Traffic data fetched from ELICSAR platform.", className = 'canvas-paragraph'),
                html.Label('Machine Learning Processing', className = 'canvas-section-title'),
                html.Br(),
                html.Br(),
                html.Img(src = 'assets/Flowchart.png')
            ],
            id="offcanvas",
            title="Introduction",
            is_open=False,
            className = 'canvas-style'
        )
    ])

@app.callback(
    Output("offcanvas", "is_open"),
    Input("button-help", "n_clicks"),
    State("offcanvas", "is_open"),
)
def toggle_offcanvas(n1, is_open):
    if n1:
        return not is_open
    return is_open
